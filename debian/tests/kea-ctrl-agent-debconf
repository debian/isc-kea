#!/bin/bash

set -e

pw_file="/etc/kea/kea-api-password"
pw_secret="secret_password_${RANDOM}"
service="kea-ctrl-agent.service"

cleanup() {
    /bin/true
}

trap cleanup EXIT

override_systemd_throttling() {
    mkdir -p /run/systemd/system/kea-ctrl-agent.service.d
    cat > /run/systemd/system/kea-ctrl-agent.service.d/override.conf <<EOF
[Unit]
StartLimitIntervalSec=0
EOF
    systemctl daemon-reload
}

check_perms() {
    local file="${1}"
    local wanted_perms="${2}"
    local perms

    perms=$(stat -c %U:%G:%a "${file}")
    if [ "${perms}" != "${wanted_perms}" ]; then
        echo "## ERROR: permissions are ${perms} and should be ${wanted_perms}"
        return 1
    else
        echo "## OK, permissions are ${perms}"
    fi
}

service_status_must_be() {
    local service_status
    local wanted_status="${1}"
    service_status=$(systemctl is-active "${service}" || /bin/true)
    systemctl status "${service}" || /bin/true
    if [ "${service_status}" != "${wanted_status}" ]; then
        echo "## ERROR, service is ${service_status}"
        return 1
    else
        echo "## OK, service is ${service_status}"
    fi
}

reconfigure_unconfigured() {
    debconf-set-selections << EOF
kea-ctrl-agent kea-ctrl-agent/make_a_choice select unconfigured
EOF
    dpkg-reconfigure kea-ctrl-agent
}

reconfigure_password() {
    local password="${1}"
    debconf-set-selections << EOF
kea-ctrl-agent kea-ctrl-agent/make_a_choice select configured_password
kea-ctrl-agent kea-ctrl-agent/kea_api_password password ${password}
kea-ctrl-agent kea-ctrl-agent/kea_api_password_again password ${password}
EOF
    dpkg-reconfigure kea-ctrl-agent
}

reconfigure_random() {
    debconf-set-selections << EOF
kea-ctrl-agent kea-ctrl-agent/make_a_choice select configured_random_password
EOF
    dpkg-reconfigure kea-ctrl-agent
}

test_fresh_install() {
    echo
    echo "## Running ${FUNCNAME[0]}"
    # On a fresh install, which is the situation we are in as this is the first
    # test being run, there is no kea-api-password file, and the service isn't
    # running
    echo "## Fresh install, default options, there must be no ${pw_file} file"
    ls -la "$(dirname ${pw_file})"
    test ! -f "${pw_file}"

    echo
    echo "## With no ${pw_file}, the service must not be running"
    service_status_must_be inactive
    echo
}

test_service_wont_start_without_pwfile() {
    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## With no ${pw_file}, service must not start"
    ls -la "$(dirname ${pw_file})"
    test ! -f "${pw_file}"
    echo "## Current status:"
    systemctl status "${service}" || /bin/true
    echo
    echo "## Attempting to start ${service}"
    systemctl start "${service}"
    service_status_must_be inactive
    echo
}

test_configured_password() {
    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## Reconfiguring kea-ctrl-agent with password ${pw_secret}"
    reconfigure_password "${pw_secret}"

    echo "## Checking that ${pw_file} exists and has ${pw_secret}"
    ls -la "$(dirname ${pw_file})"
    test -f "${pw_file}"
    generated_pw=$(cat "${pw_file}")
    if [ "${generated_pw}" != "${pw_secret}" ]; then
        echo "## ERROR, password from ${pw_file} is not equal to ${pw_secret}: ${generated_pw}"
        return 1
    else
        echo "## OK, password from ${pw_file} is ${generated_pw}"
    fi

    echo "## Checking that ${pw_file} has expected permissions and ownership"
    check_perms "${pw_file}" "root:_kea:640"
    echo

    echo
    echo "## Checking that the service is running"
    service_status_must_be active
}

test_configured_random_password() {
    local generated_pw

    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## Reconfiguring kea-ctrl-agent with random password option"
    reconfigure_random

    echo "## Checking that ${pw_file} exists and has a password different from ${pw_secret}"
    ls -la "$(dirname ${pw_file})"
    test -f "${pw_file}"

    generated_pw=$(cat "${pw_file}")
    if [ "${generated_pw}" = "${pw_secret}" ]; then
        echo "## ERROR, generated random password \"${generated_pw}\" is equal to \"${pw_secret}\""
        return 1
    else
        echo "## OK, generated random password is \"${generated_pw}\""
    fi
    echo
    echo "## Checking that ${pw_file} has expected permissions and ownership"
    check_perms "${pw_file}" "root:_kea:640"
    echo

    echo
    echo "## Checking that the service is running"
    service_status_must_be active
}

test_unconfigured() {
    local -r new_secret="${pw_secret}${pw_secret}"
    local contents

    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## Reconfiguring kea-ctrl-agent with option \"unconfigured\" should leave things as they were"
    echo
    echo "## Overwriting ${pw_file} with ${new_secret}"
    printf "%s" "${new_secret}" > "${pw_file}"

    echo "## Reconfiguring"
    reconfigure_unconfigured

    echo
    echo "## ${pw_file} should still contain ${new_secret}"
    contents=$(cat "${pw_file}")
    if [ "${contents}" != "${new_secret}" ]; then
        echo "## ERROR, ${pw_file} now contains \"${contents}\""
        return 1
    else
        echo "## OK, same content"
    fi

    echo "## Removing ${pw_file} and reconfiguring, a new one should not be created, and the service must be stopped"
    rm -f "${pw_file}"
    ls -la $(dirname "${pw_file}")
    echo "## Reconfiguring"
    reconfigure_unconfigured

    echo "## ${pw_file} was not recreated"
    ls -la $(dirname "${pw_file}")
    test ! -f "${pw_file}"
    echo "## With no ${pw_file}, the service must not be running"
    service_status_must_be inactive
}

test_no_start_with_empty_password() {
    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## kea-ctrl-agent must not start with an empty password file"
    echo
    echo "## Truncating ${pw_file}"
    truncate -s 0 "${pw_file}"
    ls -la $(dirname "${pw_file}")
    test ! -s "${pw_file}"
    echo
    echo "## Restarting kea-ctrl-agent"
    systemctl restart "${service}"
    echo
    echo "## Service must not be started"
    service_status_must_be inactive
}

test_empty_password_via_debconf() {
    local service_status
    local contents

    echo
    echo "## Running ${FUNCNAME[0]}"
    echo "## Reconfiguring with password set to ${pw_secret}"
    reconfigure_password "${pw_secret}"

    echo
    echo "## ${pw_file} must now contain ${pw_secret}"
    contents=$(cat "${pw_file}")
    if [ "${contents}" != "${pw_secret}" ]; then
        echo "## ERROR, ${pw_file} now contains \"${contents}\""
        return 1
    else
        echo "## OK, same content"
    fi

    echo
    echo "## Service must be running"
    service_status_must_be active

    echo
    echo "## Reconfiguring with an empty password should not change the existing password"
    # set an empty password (no args)
    reconfigure_password
    ls -la $(dirname "${pw_file}")
    contents=$(cat "${pw_file}")
    if [ "${contents}" != "${pw_secret}" ]; then
        echo "## ERROR, ${pw_file} now contains \"${contents}\""
        return 1
    else
        echo "## OK, same content"
    fi

    echo
    echo "## Service must be running"
    service_status_must_be active
}


# we restart kea-ctrl-agent a lot during this test
override_systemd_throttling

test_fresh_install
test_service_wont_start_without_pwfile
test_configured_password
test_configured_random_password
test_unconfigured
test_no_start_with_empty_password
test_empty_password_via_debconf
