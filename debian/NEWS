isc-kea (2.2.0-8) unstable; urgency=medium

  Require user authentication to access the kea-ctrl-agent API service.

  Upgrades from previous versions, or fresh installs, will get a debconf
  "high" priority prompt with 3 options:
  - no action (default)
  - configure with a random password
  - configure with a given password

  If there is no password, the kea-ctrl-agent will NOT start.

  The password is expected to be in /etc/kea/kea-api-password, with ownership
  root:_kea and permissions 0640. To change it, run `dpkg-reconfigure
  kea-ctrl-agent` (which will present the same 3 options from above again), or
  just edit the file manually.

 -- Andreas Hasenack <andreas@canonical.com>  Fri, 17 Mar 2023 11:28:49 -0300

isc-kea (2.2.0-3) unstable; urgency=medium

  Starting with this upload, all the kea services are confined by default with
  apparmor (if it's enabled on the host).

 -- Paride Legovini <paride@debian.org>  Fri, 17 Feb 2023 19:59:43 +0100

isc-kea (2.2.0-2) unstable; urgency=medium

  The control sockets were moved to /run/kea (Closes: #1014929)

  keactrl is no longer being installed. This script is not systemd-aware and
  not installed by the upstream .deb packages.

  Default logging of all kea services is set to "output" in their respective
  configuration files. This means they end up in the systemd journal logging.
  (Closes: #1016747)

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Tue, 14 Feb 2023 11:24:58 -0300

